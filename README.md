# Scientific Computing with Python #
### Christian Ott and Joey Fedrow ###

Lecture notes, Jupyter notebooks, and exercises for the YITP lecture series
Python Party: An Introduction to Scientific Computing with Python, May/June 2017.

Contact: cott@tapir.caltech.edu

### This lecture series is over. See below for materials. ###


## Lecture 1: Overview and First Hands-On Exercises ##
### May 2, 2017 ###

[ Lecture 1 PDF slides ](https://bitbucket.org/hypercott/python-yitp/raw/da80b0bf83b4989e8aaadce7c68b0c77dd19d961/lecture1/lecture1.pdf)

[ Lecture 1 Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/da80b0bf83b4989e8aaadce7c68b0c77dd19d961/lecture1/lecture1.ipynb) (download & save)

[ Lecture 1 Exercises ](https://bitbucket.org/hypercott/python-yitp/raw/da80b0bf83b4989e8aaadce7c68b0c77dd19d961/lecture1/lecture1_homework.ipynb) (download & save)

[ Lecture 1 Solutions](https://bitbucket.org/hypercott/python-yitp/raw/08f6a078d272cd38ce753a780d5c7836eefbf376/lecture1/Lecture1_homework_solutions.ipynb) (download & save)

## Lecture 2: NumPy and Matplotlib -- handling data and making plots ##
### May 23, 2017 ###

[ Lecture 2 PDF slides ](https://bitbucket.org/hypercott/python-yitp/raw/4ed9cb9f9a228f0f1df94988f2b943c4b393f3b4/lecture2/lecture2.pdf)

[ Lecture 2 Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/4ed9cb9f9a228f0f1df94988f2b943c4b393f3b4/lecture2/lecture2.ipynb) (download & save)

[ Lecture 2 PDF of Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/59df178d6f1d64753c6d3dabbf0e696b63349d0a/lecture2/lecture2_notebook.pdf)

[ Lecture 2 Exercises ](https://bitbucket.org/hypercott/python-yitp/raw/4ed9cb9f9a228f0f1df94988f2b943c4b393f3b4/lecture2/lecture2_homework.ipynb) (download & save)

[ Lecture 2 Solutions ](https://bitbucket.org/hypercott/python-yitp/raw/08f6a078d272cd38ce753a780d5c7836eefbf376/lecture2/Lecture2_homework_solutions.ipynb) (download & save)

[ Lecture 2 Data File 1](https://bitbucket.org/hypercott/python-yitp/raw/4ed9cb9f9a228f0f1df94988f2b943c4b393f3b4/lecture2/ceph2.txt) (download & save)

[ Lecture 2 Data File 2](https://bitbucket.org/hypercott/python-yitp/raw/4ed9cb9f9a228f0f1df94988f2b943c4b393f3b4/lecture2/bbh_gw.dat) (download & save)


## Lecture 3: Making high-quality plots with Matplotlib ##
### May 30, 2017 ###

[ Lecture 3 PDF slides ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/lecture3.pdf)

[ Lecture 3 Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/lecture3.ipynb) (download & save)

[ Lecture 3 PDF of Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/lecture3_notebook.pdf)

[ Lecture 3 Exercises ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/lecture3_homework.ipynb) (download & save)

[ Lecture 3 PDF of Exercises ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/lecture3_homework.pdf) (download & save)

[ plot_defaults.py ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/plot_defaults.py) (download & save)

[ Lecture 3 Image File ](https://bytebucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/BBH_orbit.png) (download & save)

[ Lecture 3 Data File 1](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/bbh_gw.dat) (download & save)

[ Lecture 3 Data File 2](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/BBH_ah1.gp) (download & save)

[ Lecture 3 Data File 3](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/BBH_ah2.gp) (download & save)

[ Lecture 3 Data File 4](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/rho_c_t.dat) (download & save)

## Lecture 4: More on plotting and analyzing data with SciPy ##
### June 6, 2017 ###

[ Lecture 5 Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/lecture4_homework.ipynb) (download & save)

[ Lecture 4 PDF of Jupyter Notebook ](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/lecture4_notebook.pdf)

[ Lecture 4 Exercises ](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/lecture4_homework.ipynb) (download & save)

[ Lecture 4 PDF of Exercises ](https://bitbucket.org/hypercott/python-yitp/src/a94648995bae249b7892b387639c40165a83f20e/lecture4/lecture4_homework.pdf?at=master) (download & save)

[ plot_defaults.py ](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/plot_defaults.py) (download & save)

[ Lecture 4 Data File 1](https://bitbucket.org/hypercott/python-yitp/raw/132d9563583d3751efbbf72011a3b4d854058c08/lecture3/bbh_gw.dat) (download & save)

[ Lecture 4 Data File 2](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/interdata.dat) (download & save)

[ Lecture 4 Data File 3](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/A634w5.50_LS220_A.dat) (download & save)

[ Lecture 4 Data File 4](https://bitbucket.org/hypercott/python-yitp/raw/a94648995bae249b7892b387639c40165a83f20e/lecture4/fitdata.txt) (download & save)